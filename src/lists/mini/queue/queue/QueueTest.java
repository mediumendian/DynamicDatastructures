package queue;

import java.util.stream.IntStream;

import adapter.QueueAdapter;

/* assert in Ringpuffer implementation requires the -ea (enable assertions) flag for jvm.
 * However, it also works fine without.
 * In eclipse go to 'Run → Run conf → arguments tab → vm options field
 *
 * IntStream requires the Java 8 Stream API. For this:
 * The Java 8 JRE has to be present and enabled
 * The compiler compliance level has to be set to 1.8
 */

public class QueueTest {

	public static void main(String[] args) {
		QueueAdapter q = new QueueAdapter(2);
		IntStream.range(0, 10).forEach(i -> q.offer(i));
		IntStream.range(0, 10).forEach(i -> System.out.println(q.poll()));

		System.out.println(q.peek());

		IntStream.range(0, 20).forEach(i -> System.out.print(q.poll() + ", "));
	}
}

/* 0
 * 0, 1, 4, 9, 16, 25, 36, 49, 64, 81, 100, 121, 144, 169, 196, 225, 256, 289, 324, 361, 
 */