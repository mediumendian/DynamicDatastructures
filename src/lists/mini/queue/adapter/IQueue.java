package adapter;

public interface IQueue {
	public boolean isEmpty();
	public int getSize();
	public void offer(int element);
	public int peek();
	public int poll();
}
