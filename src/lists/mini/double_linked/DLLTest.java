package double_linked;

import java.util.stream.IntStream;

/* IntStream requires the Java 8 Stream API. For this:
 * The Java 8 JRE has to be present and enabled
 * The compiler compliance level has to be set to 1.8
 */

public class DLLTest {
	public static void main(String[] args) {
		DLList<Integer> test = new DLList<>();
		IntStream.range(0, 11).forEach(i -> test.add(i));
		System.out.println(test.toString());
		IntStream.of(3, 6).forEach(i -> test.remove(i));
		IntStream.of(4, 7).forEach(i -> test.get(i));
		System.out.println(test);
	}
}

/*
 * # of elements is 11. 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
 * # of elements is 9.  0, 1, 2, 4, 5, 6, 8, 9, 10
 */