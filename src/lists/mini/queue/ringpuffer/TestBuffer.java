package ringpuffer;

public class TestBuffer {

	public static void main(String[] args) {
		int j;
		Ringpuffer r1 = new Ringpuffer(10);

		for (int i = 0; i < 16; i++) {
			r1.offer(i);
		}
		System.out.println("offered 16");
		for (int i = 0; i < 5; i++) {
			j = r1.poll(); // for debugging; debugger prints "exhaustingly thorough"
			System.out.print(j + ", ");
		}
		System.out.println("\npolled 5");
		for (int i = 0; i < 25; i++) {
			r1.offer(i);
		}
		System.out.println("offered 25");
		for (int i = 0; i < 30; i++) {
			j = r1.poll();
			System.out.print(j + ", ");
		}
		System.out.println("\npolled 30");
	}
}

/* offered 16
 * 0, 1, 2, 3, 4,
 * polled 5
 * offered 25 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,
 * polled 30
 */
